; Generated by holtek-gcc v3.19, Fri Oct 14 17:16:20 2022
; 
; Configuration:
;       with long instruction
;       Single ROM, Multi-RAM
;       bits_per_rom_unit:16
;       with mp1
;       with tbhp, address(0x9)
;          Use tabrd-const
;       
; SFR address of long-instruction arch:
;    mp0 = -1,1,0
;    mp1 = 4,3,2
;    mp2 = 14,13,12
;    tbp = 9,7,8
;    acc = 5
;    pcl = 6
;    status = 10
;    bp = 11
;    intc = 72
;       
;       
; use 'tabrdc' instead of 'tabrd'
;       

#pragma translator "holtek-gcc 4.6.4" "3.19" "build 20130711"
; Rebuild 718

ir equ [2]
mp equ [3]
sbp equ [4]
acc equ [5]
bp equ [11]
tblp equ [7]
tbhp equ [9]
status equ [10]
c equ [10].0
ac equ [10].1
z equ [10].2
ov equ [10].3
cz equ [10].6
sc equ [10].7
intc equ [72]

extern ra:byte
extern rb:byte
extern rc:byte
extern rd:byte
extern re:byte
extern rf:byte
extern rg:byte
extern rh:byte
extern _Crom2Prom:near
extern _Crom2PromNext:near

RAMBANK 0 @BITDATASEC, @BITDATASEC1
@HCCINIT	.section 'data'
@HCCINIT0	.section 'data'
@BITDATASEC	 .section 'data'
@BITDATASEC1	 .section 'data'

#pragma debug scope 1 1
	extern __DELAY3:near
	extern __DELAYX3:near
	extern __DELAYX6:near
	extern __DELAYY5:near
	extern __DELAYY3:near
	extern _builtin_holtek_delay_2:byte
public _Button_State_Init
#pragma debug scope 2 1
#line 27 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
_Button_State_Init .section 'code'
_Button_State_Init proc
#line 28 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _button_state[0].1
#line 29 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _button_state[0].3
#line 30 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _Button_Long_Press_CNT[0]
	clr _Button_Long_Press_CNT[1]
	ret
_Button_State_Init endp
#line 30 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
#pragma debug scope 1 1
___pa equ [20]
public _Button_State_Update
#pragma debug scope 3 1
#line 41 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
_Button_State_Update .section 'code'
_Button_State_Update proc
#line 44 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	sz [20].7
	jmp _L3
#line 46 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	set _button_state[0].0
#line 48 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	sz _button_state[0].2
	jmp _L5
#line 50 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,1
	add a,_Button_Long_Press_CNT[0]
	mov rb,a
	mov a,0
	adc a,_Button_Long_Press_CNT[1]
	mov rc,a
	mov a,rb
	mov _Button_Long_Press_CNT[0],a
	mov a,rc
	mov _Button_Long_Press_CNT[1],a
#line 51 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,208
	sub a,rb
	mov a,3
	sbc a,rc
	sz c
	jmp _L5
#line 53 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _Button_Long_Press_CNT[0]
	clr _Button_Long_Press_CNT[1]
#line 54 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	set _button_state[0].2
#line 55 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	set _task_priority[0].5
_L5:
#line 59 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	sz _button_state[0].3
	jmp _L2
#line 61 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	inca ___debounce_cnt_1922[0]
	mov ra,a
	mov a,ra
	mov ___debounce_cnt_1922[0],a
#line 62 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,4
	sub a,ra
	sz c
	jmp _L2
#line 64 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	set _button_state[0].3
	jmp _L2
_L3:
#line 71 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr ___debounce_cnt_1922[0]
#line 72 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _button_state[0].0
#line 73 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _button_state[0].3
	jmp _L2
#line 75 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _button_state[0].3
#line 76 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	set _button_state[0].1
#line 77 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _button_state[0].2
#line 78 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _Button_Long_Press_CNT[0]
	clr _Button_Long_Press_CNT[1]
_L2:
	ret
_Button_State_Update endp
#line 78 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
#pragma debug scope 1 1
___wdtc equ [61]
@crom	.section 'crom'
_LC0:
	db 04ch,042h,048h,0dh,00h
public _Button_Process
#pragma debug scope 4 1
#line 90 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
_Button_Process .section 'code'
_Button_Process proc
    local _Button_Process_2 db 1 dup(?)	; 0,1
#line 92 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _button_state[0].1
	jmp _L14
#line 94 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _task_priority[0].5
	jmp _L16
#line 96 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _task_priority[0].5
	jmp _L18
_L16:
#line 101 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _task_priority[0].0
	jmp _L17
#line 103 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	set _task_priority[0].1
#line 104 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _task_priority[0].0
#line 105 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	set _flag_horn_alarm_bits[0].3
#line 106 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,50
	mov _Diagnostic_Event_Update_2[2],a
	jmp _L49
_L17:
#line 108 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _task_priority[0].1
	jmp _L19
_L20:
#line 110 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Horn_Chirp_2[0],a
	clr _Horn_Chirp_2[1]
	call _Horn_Chirp
	jmp _L18
_L19:
#line 112 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	sz _task_priority[0].2
	jmp _L20
#line 116 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _task_priority[0].3
	jmp _L21
	mov a,4
	mov _Button_Process_2[0],a
_L23:
#pragma debug scope 5 4
;begin block, line: 116.-1
#line 121 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Red_LED_Blink_2[0],a
	clr _Red_LED_Blink_2[1]
	call _Red_LED_Blink
#line 122 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,94
	mov _Delay_Milliseconds_2[0],a
	mov a,1
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
	sdz _Button_Process_2[0]
	jmp _L23
#line 124 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,32
	mov _Delay_Milliseconds_2[0],a
	mov a,3
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
#line 125 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr [61]
_L24:
	jmp _L24
_L21:
#pragma debug scope 4 1
#line 128 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _task_priority[0].4
	jmp _L25
#line 130 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Horn_Chirp_2[0],a
	clr _Horn_Chirp_2[1]
	call _Horn_Chirp
#line 131 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,32
	mov _Delay_Milliseconds_2[0],a
	mov a,3
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
#line 132 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr [61]
_L26:
	jmp _L26
_L25:
#line 135 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _task_priority[0].6
	jmp _L27
	sz _task_priority[0].2
	jmp _L27
#pragma debug scope 6 4
;begin block, line: 135.-1
#line 140 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Red_LED_Blink_2[0],a
	clr _Red_LED_Blink_2[1]
	call _Red_LED_Blink
#line 141 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,94
	mov _Delay_Milliseconds_2[0],a
	mov a,1
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
#line 140 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Red_LED_Blink_2[0],a
	clr _Red_LED_Blink_2[1]
	call _Red_LED_Blink
#line 141 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,94
	mov _Delay_Milliseconds_2[0],a
	mov a,1
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
#line 143 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _task_priority[0].6
	jmp _L50
_L27:
#pragma debug scope 4 1
#line 146 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	sz _task_priority[0].7
	jmp _L28
	snz _task_priority[1].1
	jmp _L29
_L28:
#line 148 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _flag2[0].3
	jmp _L30
#line 150 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	sz _task_priority[1].1
	jmp _L30
#line 152 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	set _task_priority[1].1
#line 153 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _task_priority[0].7
#line 154 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr __1_min_bat_hush_cnt[0]
	clr __1_min_bat_hush_cnt[1]
#line 155 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,low offset _LC0
	mov _Uart_Send_String_2[0],a
	mov a,high offset _LC0
	mov _Uart_Send_String_2[1],a
	call _Uart_Send_String
_L30:
#line 158 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Horn_Chirp_LED_Blink_2[0],a
	clr _Horn_Chirp_LED_Blink_2[1]
	call _Horn_Chirp_LED_Blink
	jmp _L18
_L29:
#line 160 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _task_priority[1].2
	jmp _L31
	sz _task_priority[0].2
	jmp _L31
#pragma debug scope 7 4
;begin block, line: 160.-1
#line 165 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Red_LED_Blink_2[0],a
	clr _Red_LED_Blink_2[1]
	call _Red_LED_Blink
#line 166 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,94
	mov _Delay_Milliseconds_2[0],a
	mov a,1
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
#line 165 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Red_LED_Blink_2[0],a
	clr _Red_LED_Blink_2[1]
	call _Red_LED_Blink
#line 166 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,94
	mov _Delay_Milliseconds_2[0],a
	mov a,1
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
_L50:
#line 168 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr ra
	snz _task_priority[1].2
	set ra.2
	clr _task_priority[1].2
	mov a,ra
	orm a,_task_priority[1]
#pragma debug scope 4 1
#line 161 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	jmp _L18
_L31:
#line 170 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _task_priority[1].0
	jmp _L32
#line 172 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _task_priority[1].0
#line 173 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Horn_Chirp_2[0],a
	clr _Horn_Chirp_2[1]
	call _Horn_Chirp
#line 174 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,32
	mov _Delay_Milliseconds_2[0],a
	mov a,3
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
	jmp _L18
_L32:
#line 176 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	snz _flag3[0].4
	jmp _L33
#pragma debug scope 8 4
;begin block, line: 176.-1
#line 178 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Horn_Chirp_2[0],a
	clr _Horn_Chirp_2[1]
	call _Horn_Chirp
	mov a,7
	mov _Button_Process_2[0],a
_L35:
#line 181 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,30
	mov _Red_LED_Blink_2[0],a
	clr _Red_LED_Blink_2[1]
	call _Red_LED_Blink
#line 182 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,94
	mov _Delay_Milliseconds_2[0],a
	mov a,1
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
#line 183 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
;# 183 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c" 1
	clr wdt
	sdz _Button_Process_2[0]
	jmp _L35
#line 186 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,32
	mov _Delay_Milliseconds_2[0],a
	mov a,3
	mov _Delay_Milliseconds_2[1],a
	call _Delay_Milliseconds
#line 187 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr [61]
_L36:
	jmp _L36
_L33:
#pragma debug scope 4 1
#line 190 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	sz _task_priority[1].3
	jmp _L18
#line 192 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	set _task_priority[1].3
#line 193 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _flag1[0].0
#line 194 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	mov a,63
	mov _Diagnostic_Event_Update_2[2],a
_L49:
	mov a,__life_cycles[0]
	mov _Diagnostic_Event_Update_2[0],a
	mov a,__life_cycles[1]
	mov _Diagnostic_Event_Update_2[1],a
	call _Diagnostic_Event_Update
_L18:
#line 196 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
	clr _button_state[0].1
_L14:
	ret
_Button_Process endp
#line 196 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
#pragma debug scope 1 1
@HCCINIT	.section 'data'
#line 42 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
@HCCINIT___debounce_cnt_1922 .section 'data'
___debounce_cnt_1922 label byte
#pragma debug array 42 8 2 1
#pragma debug variable 42 3 ___debounce_cnt_1922 "debounce_cnt"
	db 0
@ROMDATA_BASE .section inpage 'code'
	db 0
@HCCINIT___debounce_cnt_1922 .section 'data'
	db 0
@ROMDATA_BASE .section inpage 'code'
	db 0
@HCCINIT___debounce_cnt_1922 .section 'data'
public _Button_Long_Press_CNT
#line 18 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
@HCCINIT_Button_Long_Press_CNT .section 'data'
_Button_Long_Press_CNT label byte
#pragma debug variable 12 1 _Button_Long_Press_CNT "Button_Long_Press_CNT"
	db 0
@ROMDATA_BASE .section inpage 'code'
	db 0
@HCCINIT_Button_Long_Press_CNT .section 'data'
	db 0
@ROMDATA_BASE .section inpage 'code'
	db 0
@HCCINIT_Button_Long_Press_CNT .section 'data'
public _button_state
#line 16 "D:\Porject\Stowe\Code\Debug\revew\code\C\button.c"
@HCCINIT_button_state .section 'data'
_button_state label byte
#pragma debug struct_begin 43 "bit_type"
#pragma debug field 21 8 0 1 "bit0" ;0,1
#pragma debug field 21 8 0 1 "bit1" ;1,1
#pragma debug field 21 8 0 1 "bit2" ;2,1
#pragma debug field 21 8 0 1 "bit3" ;3,1
#pragma debug field 21 8 0 1 "bit4" ;4,1
#pragma debug field 21 8 0 1 "bit5" ;5,1
#pragma debug field 21 8 0 1 "bit6" ;6,1
#pragma debug field 21 8 0 1 "bit7" ;7,1
#pragma debug struct_end
#pragma debug union_begin 44 ""
#pragma debug field 2 43 "bits"
#pragma debug field 0 8 "byte"
#pragma debug union_end
#pragma debug variable 44 1 _button_state "button_state"
	db 0
@ROMDATA_BASE .section inpage 'code'
	db 0
@HCCINIT_button_state .section 'data'
#pragma debug struct_begin 45 "__pa_bits"
#pragma debug field 21 8 0 1 "__pa0" ;0,1
#pragma debug field 21 8 0 1 "__pa1" ;1,1
#pragma debug field 21 8 0 1 "__pa2" ;2,1
#pragma debug field 21 8 0 1 "__pa3" ;3,1
#pragma debug field 21 8 0 1 "__pa4" ;4,1
#pragma debug field 21 8 0 1 "__pa5" ;5,1
#pragma debug field 21 8 0 1 "__pa6" ;6,1
#pragma debug field 21 8 0 1 "__pa7" ;7,1
#pragma debug struct_end
#pragma debug union_begin 46 ""
#pragma debug field 2 45 "bits"
#pragma debug field 0 8 "byte"
#pragma debug union_end
#pragma debug variable 46 1 ___pa "__pa" 1
#pragma debug struct_begin 47 "__wdtc_bits"
#pragma debug field 21 8 0 1 "__ws0" ;0,1
#pragma debug field 21 8 0 1 "__ws1" ;1,1
#pragma debug field 21 8 0 1 "__ws2" ;2,1
#pragma debug field 21 8 0 1 "__we0" ;3,1
#pragma debug field 21 8 0 1 "__we1" ;4,1
#pragma debug field 21 8 0 1 "__we2" ;5,1
#pragma debug field 21 8 0 1 "__we3" ;6,1
#pragma debug field 21 8 0 1 "__we4" ;7,1
#pragma debug struct_end
#pragma debug union_begin 48 ""
#pragma debug field 2 47 "bits"
#pragma debug field 0 8 "byte"
#pragma debug union_end
#pragma debug variable 48 1 ___wdtc "__wdtc" 1

; output external variables
extern __life_cycles:byte
extern _flag1:byte
extern _flag3:byte
extern __1_min_bat_hush_cnt:byte
extern _flag2:byte
extern _flag_horn_alarm_bits:byte
extern _task_priority:byte
extern _Diagnostic_Event_Update_2:byte
extern _Diagnostic_Event_Update:near
extern _Horn_Chirp_2:byte
extern _Horn_Chirp:near
extern _Red_LED_Blink_2:byte
extern _Red_LED_Blink:near
extern _Delay_Milliseconds_2:byte
extern _Delay_Milliseconds:near
extern _Uart_Send_String_2:byte
extern _Uart_Send_String:near
extern _Horn_Chirp_LED_Blink_2:byte
extern _Horn_Chirp_LED_Blink:near

; 
; Generated by holtek-gcc v3.19, Fri Oct 14 17:16:20 2022
; Rebuild 718
; end of file
