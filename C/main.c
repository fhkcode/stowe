/**************************************************************************************************************
 * The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
* 
*
* Model name: Stowe
* Market: UK
* Battery: 9V carbon zinc battery, 1 year battery warranty..
* Description: this's a very basical smoke alarm, only has smoke detection, no EOL, interconnect function.
*
* Features included:
* 	1. Detect smoke to alarm, sample period 10 seconds.
* 	2. Hush the alarm, and hush timeout set to 9 min.
* 	3. Compensation every 2 hours, add/minus 1 count each time.
*	4. Photo chamber supervision
*	5. Push-To-test function, IRED drive current set to 300mA
*	6. PTT fault supervision
*	7. Battery test: low battery warning, battery warning hush, battery fatal.
*	8. Memory fault, EEPROM fault 
*	9. Self calibration
*	10. Stuck button warning.
*	11. No EOL, No interconnect functions.
*	12. Debug interface: Send serial command to revise the alarm threhsold, IRED current etc.
*	13. Watchdog 
***************************************************************************************************************/ 

#include "BA45F5440.h"
#include "sys.h"
#include "photo.h"
#include "board.h"
#include "button.h"
#include "horn.h"
// #include "led.h"
#include "adc.h"
#include "uart.h"
#include "eeprom.h"
#include "battery.h"

void _Idle_0(void);
void _Idle_1(void);
void Power_Save(void);
void LVD_Disable(void);

u8 _1_sec_counter = 0;



void main()
{
	System_Init();	/*Setup the main clock to 8MHz, watchdog overflow to 2 sec	*/
	Uart_Init();
	Delay_Milliseconds(1000);	/*Delay 1000ms to wait power supply being stable, in case data corruption when writing data to non-volatile memory.*/
	
	Horn_Init();	//NOTE: for the 3 pin pizeo, we will use PA6 to enable the horn, when PA6 = 1, horn enable, when PA6 = 0, horn disable.
	ADC_Init();		// ADC initialization
	Load_Non_Volatile_data();
	Opamp_Init();	
	Opamp_0_Calibration();	// OPAMP_0, OPAMP_1 calibration, per the datasheet, the 2 interla OPAMPs have to		
	Opamp_1_Calibration();	// be calibrated before using.
	Board_Init();
	Photo_Params_Init();	//Smoke related parameters initialization
	Button_State_Init();
	Diagnostic_Event_Init();
	FLAG_POWER_ON = TRUE;
	
	ROM_Data_Validate();
	Uart_Power_On_Output(); // ANCHOR  Power output model part number, FW revision, supervision, alarm thresholds.
	Power_On_Blink();
	LVD_Disable();
	while(TRUE)
	{
		GCC_CLRWDT(); //REVIEW : Feed dog, clear watchdog timer.	
		
		//tb1 8ms
		//NOTE : tb1 is a peridical interruption, source clock is from 32KHz oscillator.
		//		 Period set to 8ms, mainly function is to give a alarm pattern signal (T3 horn pattern and LED blinks)
		_tb1c  = 0b10000000;

		//tb0 1.024s
		//NOTE : tb0 is a peridical interruption, source clock is from 32KHz oscillator.
		//		 Period set to 1.024 (32768 dived by 32000), mainly function is to give a alarm pattern signal (T3 horn pattern and LED blinks)
		// 		[bit3:bit0]: time base 0/1 overflow setting, "111" is a 2^15 devider.
		_tb0c  = 0b10000111;								
		
		//8ms period
		if(_tb1f)				
		{
			_tb1f = 0;
			if(FLAG_IN_CAL_STATUS == FALSE)	// NOTE in calibration process, disable button press.
			{
				Button_State_Update();	
			}				
			Button_Process();
			Horn_T3_Pattern();
									
		}//NOTE: end of 8ms period loop
		
		if(_tb0f)	//1 sec period, _tb0f interrupt flag of time base 0			
		{
			_tb0f = 0;
										
			_1_sec_counter++;			
			
			if(FLAG_IDLE0_ACTIVE) 	//REVIEW: No valid activity, put the alarm into sleep mode, photo sampling 1 count/10sec.
			{
				photo_sampling_cnt++;
			}
			else if(!FLAG_T3_ALARM_ON)		/*DO NOT start a photo sampling when sounder is ON, vlot change will cause ADC count change (6~8 ADC count).*/
			{
				photo_sampling_cnt = PHOTO_SAMPLE_PERIOD;	//REVIEW: alarm not in sleep mode, activity alive, for the photo sample to 1 count/sec.
			}
				
			if(photo_sampling_cnt >= PHOTO_SAMPLE_PERIOD)
			{	
				photo_sampling_cnt = 0;	
				
				Read_Photo_Output();
				
				if (FLAG_PHOTO_CAL)
				{
					Smoke_Alarm_Check();	/*Alarm detection, if a photo output is over the alarm threshold with 5 successive count, alarm.*/			
				}	 									
			}
			
			if((!FLAG_SMOKE_ALARM)&&(!FLAG_PTT_ACTIVE))
			{
				Drift_Compensation();	/*DO NOT do compensation during alarm condition, otherwise the alarm will be overrided after compensation. */
				Battery_Test();		/* battery test will pull down the volt, affects the audibility output, avoid battery test in alarm condition. */
			}
			
			Task_Sched();
			Uart_CMD_Process();
			Life_Update();
			
			
		}
		
		//jude mode , if not something happen ,then go to sleep,to save battery power
		Power_Save();	
	}
}// NOTE: end of main loop



void __attribute((interrupt(0x10))) Uart_Interrupt(void)
{
	Uart_Receive_Process();
	FLAG_RX_ACTIVE = TRUE;
}


void __attribute((interrupt(0x14))) Lvr_Interrupt(void)
{
	if (_lvf == 1)
     {
        _lvf = 0;
        _wrf = 1;
        if(_lvdo == 1)
        {
        	_wdtc = 0;	// Watchdog reset.
			while(TRUE);	
        }
       /* user define */
     }
}

/***********************************************************************************************************
											_Idle_0
*Description:	this function will shunt down the power of core, peripherals and the 8MHz RC oscillator, only
				keep the 32KHz low frequency oscillator on, just to save the battery life.
*Arguments: None
*Return: 	None
***********************************************************************************************************/
void _Idle_0(void){
	_fhiden = 0;
	_fsiden = 1;
}

/***********************************************************************************************************
											_Idle_1
*Description:	this function is to keep the 8MHz main oscillator on, but still shunt down the power of core, 
				peripherals. The purpose is to give  a fast response on task processing.
*Return: 	None
***********************************************************************************************************/
void _Idle_1(void){
	_fhiden = 1;	// 8MHz oscillator on.
	_fsiden = 1;
}

/***********************************************************************************************************
											Power_Save
*Description:	1. Activity alive, do not close the 8MHz main clock, but shunt down the CPU, ADC etc.
				2. No activities, force the alarm into idle 0, power off 8MHz main clock, shunt down the CPU, 
				IOs, ADC etc. only keeps the low frequency oscillator 32KHz working.
				Below bit will be checked if true or not, to determine if idle0 or idle1 is using.
				
				0xD803=??1101100000000011??
				FLAG_SMOKE_ALARM			1
				FLAG_HUSH_ACTIVE			1
				FLAG_EOL_FATAL				0	
				FLAG_PHOTO_FAULT			0
				FLAG_MEMORY_FAULT			0
				FLAG_BTN_STUCK				0
				FLAG_EOL					0
				FLAG_BATTERY_LOW			0
				FLAG_ALARM_MEMORY			0
				FLAG_BATTERY_HUSH_ACTIVE	0	
				FLAG_EOL_HUSH				0
				FLAG_PTT_ACTIVE				1	
				FLAG_IN_CAL_STATUS			1
				FLAG_PHOTO_CAL_FAILURE		0
				FLAG_PRE_ALARM				1
				FLAG_FAST_SAMPLING			1
				
*** The function determins which activity affects the photo sampling and the horn.

*Arguments: None
*Return: 	None
***********************************************************************************************************/
void Power_Save(void){
	if(button_state.byte || (task_priority.word & 0xD803) ||FLAG_HORN_ALARM_ENABLE)
	{
		FLAG_IDLE0_ACTIVE = FALSE;
		_tb1f=0;
		_Idle_1();
		_halt();
	}
	else{ 
		FLAG_IDLE0_ACTIVE = TRUE;
		_tb0f = 0;
		_tb1on = 0;
		if(FLAG_RX_ACTIVE){
			_Idle_1();
		}else{
			_Idle_0();
		}
		_halt();
		Button_State_Update();
	}
}

/***********************************************************************************************************
											_LVD disable
*Description:	.
*Return: 	None
***********************************************************************************************************/
void LVD_Disable(void)
{
	_vbgen = 0;		//disable bandgap 
	_lvden = 0;		//disable lvd 
	_lve   = 0;		//disable lvd interrupt	
}