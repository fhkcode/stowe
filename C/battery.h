/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/
#ifndef	_BATTERY_H__
#define _BATTERY_H__

#define	    _12_MIN_TIMEOUT			    12
#define     _12_HOURS                   (u16)43200

#define     BATTERY_WARNNING_LIMIT   		2646	//2616 12bit low battery threshold = 7.5V
#define     BATTERY_FATAL_LIMIT   			2548	//12bit low batery fatal threshold = 7.4V
#define	    LOW_BAT_HUSH_TIMEOUT		(u16)2800	// 24 hours timeout, 30 sec red led blink.2880


extern u16 battery_test_count;

void Battery_Test_Current_On(void);
void Battery_Test_Current_Off(void);
unsigned int Read_Battery_Volt(void);
void Battery_Pulse(void);
void Battery_Test(void);
#endif