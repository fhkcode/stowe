/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/ 

#ifndef _BUTTON_H__
#define _BUTTON_H__

#include "BA45F5440.h"
#include "sys.h"
	
#define PA7_TEST_BUTTON		_pa7	// Test button input

//NOTE: daniel gee add:
#define BUTTON_INPUT_VALIDE     0
#define DEBOUNCE_DELAY	        5		    // Test button debounce  N*8.192
#define	LONG_PRESS_TIMEOUT	    (u16)977     // Test button long press = 977 * 8.192ms = 8.003 sec

extern byte_type button_state;
#define     BUTTON_STATE_CHANGED	    button_state.bits.bit0
#define 	FLAG_BTN_RELEASE	        button_state.bits.bit1	//button release
#define		FLAG_BTN_LONG_PRESS_STATE   button_state.bits.bit2	//button long press state
#define		FLAG_BTN_STATE_CHANGE	    button_state.bits.bit3	//button state change

void Button_State_Init(void);
void Button_State_Update(void);
void Button_Process(void);
#endif
