/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/

#include "button.h"
#include "BA45F5440.h"
#include "board.h"
#include "horn.h"
#include "eeprom.h"
#include "uart.h"

byte_type button_state = {.byte = 0};

u16 Button_Long_Press_CNT = 0;

/***********************************************************************************************************
											Button_State_Init
*Description:Test button state initialization.
*Arguments: None
*Return: 	None
***********************************************************************************************************/
void Button_State_Init(void)
{	
	FLAG_BTN_RELEASE = FALSE;
	FLAG_BTN_STATE_CHANGE = FALSE;	
	Button_Long_Press_CNT = 0;

}

/***********************************************************************************************************
											Button_State_Update
*Description:
*Arguments: None
*Return: 	None
***********************************************************************************************************/
void Button_State_Update(void)
{
	static unsigned char debounce_cnt[2] = {0};
	
	if(PA7_TEST_BUTTON == BUTTON_INPUT_VALIDE)
	{
		BUTTON_STATE_CHANGED = TRUE;
		//button long press
		if(FLAG_BTN_LONG_PRESS_STATE == FALSE)
		{
			Button_Long_Press_CNT++;
			if( Button_Long_Press_CNT >= LONG_PRESS_TIMEOUT)
			{
				Button_Long_Press_CNT = 0;
				FLAG_BTN_LONG_PRESS_STATE = TRUE;
				FLAG_BTN_STUCK = TRUE;
			}			
		}
		//button short press
		if(FLAG_BTN_STATE_CHANGE == FALSE)
		{
			++debounce_cnt[0];
			if(debounce_cnt[0] >= DEBOUNCE_DELAY)
			{
				FLAG_BTN_STATE_CHANGE = TRUE;
			}
		}		
	}
	else
	{
		//button release
		debounce_cnt[0] = 0;
		BUTTON_STATE_CHANGED = FALSE;
		if(FLAG_BTN_STATE_CHANGE)
		{
			FLAG_BTN_STATE_CHANGE = FALSE;
			FLAG_BTN_RELEASE= TRUE; 
			FLAG_BTN_LONG_PRESS_STATE = FALSE;
			Button_Long_Press_CNT = 0;
		}
	}
}

/***********************************************************************************************************
											Button_Process
*Description:	In different modes, button press will give different responses, like error code, push-to-test.
*Arguments: None
*Return: 	None
***********************************************************************************************************/
void Button_Process(void)
{
	//button release
	if(FLAG_BTN_RELEASE)
	{
		if(FLAG_BTN_STUCK)
		{
			FLAG_BTN_STUCK = FALSE;
			FLAG_BTN_RELEASE = FALSE;
		}
		else
		{
			if(FLAG_SMOKE_ALARM)
			{
				FLAG_HUSH_ACTIVE = TRUE;
				FLAG_SMOKE_ALARM = FALSE;
				FLAG_T3_ALARM_STOP = TRUE;
				Diagnostic_Event_Update(_life_cycles, LOC_MINIOR_SMOKE_HUSH);
			}
			else if(FLAG_HUSH_ACTIVE)
			{
				Horn_Chirp(LED_BLINK_WIDTH);
			}
			else if(FLAG_EOL_FATAL)
			{
				Horn_Chirp(LED_BLINK_WIDTH);	
			}
			else if(FLAG_PHOTO_FAULT)
			{
				u8 i=0;
				for(i=0;i<PHOTO_ERROR_CODE;i++)
				{
					Red_LED_Blink(LED_BLINK_WIDTH);
					Delay_Milliseconds(LED_BLINK_OFF_WIDTH);
				}
				Delay_Milliseconds(800);
				_wdtc = 0;	// Watchdog reset.
				while(TRUE);
			}
			else if (FLAG_MEMORY_FAULT)
			{
				Horn_Chirp(LED_BLINK_WIDTH);
				Delay_Milliseconds(800);
				_wdtc = 0;	// Watchdog reset.
				while(TRUE);
			}
			else if((FLAG_EOL)  && (!FLAG_EOL_FATAL))
			{
				unsigned char i=0;
				for(i=0; i< EOL_CODE; i++)
				{
					Red_LED_Blink(LED_BLINK_WIDTH);
					Delay_Milliseconds(LED_BLINK_OFF_WIDTH);
				}
				FLAG_EOL = FALSE;
				FLAG_EOL_HUSH = ~FLAG_EOL_HUSH;
			}
			else if(FLAG_BATTERY_LOW || FLAG_BATTERY_HUSH_ACTIVE)
			{
				if(FLAG_LB_HUSHABLE)
				{
					if(FLAG_BATTERY_HUSH_ACTIVE == FALSE)
					{
						FLAG_BATTERY_HUSH_ACTIVE = TRUE;
						FLAG_BATTERY_LOW = FALSE;
						_1_min_bat_hush_cnt = 0;
						Uart_Send_String("LBH\r");
					}
				}
				Horn_Chirp_LED_Blink(LED_BLINK_WIDTH);
			}
			else if((FLAG_EOL_HUSH) && (!FLAG_EOL_FATAL))
			{
				unsigned char i=0;
				for(i=0; i< EOL_CODE; i++)
				{
					Red_LED_Blink(LED_BLINK_WIDTH);
					Delay_Milliseconds(LED_BLINK_OFF_WIDTH);
				}
				FLAG_EOL_HUSH = ~FLAG_EOL_HUSH;
			}
			else if(FLAG_ALARM_MEMORY)
			{
				FLAG_ALARM_MEMORY = FALSE;
				Horn_Chirp(LED_BLINK_WIDTH);
				Delay_Milliseconds(800);
			}
			else if(FLAG_PTT_FAULT){
				unsigned char i=0;
				Horn_Chirp(LED_BLINK_WIDTH);
				for(i=0; i< PTT_ERROR_CODE; i++)
				{
					Red_LED_Blink(LED_BLINK_WIDTH);
					Delay_Milliseconds(LED_BLINK_OFF_WIDTH);
					GCC_CLRWDT(); //REVIEW : Feed dog, clear watchdog timer.
				}

				Delay_Milliseconds(800);
				_wdtc = 0;	// Watchdog reset.
				while(TRUE);
			}
			else if(FLAG_PTT_ACTIVE == FALSE)	//Push-to-Press
			{	
				FLAG_PTT_ACTIVE = TRUE;
				F_ALARM_MODE = FALSE;
				Diagnostic_Event_Update(_life_cycles, LOC_MINIOR_PTT_NUMBERS);
			}
			FLAG_BTN_RELEASE = FALSE;
		}
	}
}

