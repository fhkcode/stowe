/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/

#ifndef _SYS_H__
#define _SYS_H__

#define 	FW_REV					">Rev:3.9.5\r"	

typedef unsigned char	u8;
typedef unsigned int	u16;
typedef unsigned long	u32;
volatile typedef struct {
	unsigned char bit0 : 1;
	unsigned char bit1 : 1;
	unsigned char bit2 : 1;
	unsigned char bit3 : 1;
	unsigned char bit4 : 1;
	unsigned char bit5 : 1;
	unsigned char bit6 : 1;
	unsigned char bit7 : 1;
}bit_type;

typedef	union	{
	bit_type	bits;
		u8		byte;
}byte_type;

volatile typedef struct {
	unsigned char bit0 : 1;
	unsigned char bit1 : 1;
	unsigned char bit2 : 1;
	unsigned char bit3 : 1;
	unsigned char bit4 : 1;
	unsigned char bit5 : 1;
	unsigned char bit6 : 1;
	unsigned char bit7 : 1;
	unsigned char bit8 : 1;
	unsigned char bit9 : 1;
	unsigned char bit10 : 1;
	unsigned char bit11 : 1;
	unsigned char bit12 : 1;
	unsigned char bit13 : 1;
	unsigned char bit14 : 1;
	unsigned char bit15 : 1;
}word_bit_type;

typedef	union	{
	word_bit_type bits;
		u16	word;
}word_type;


#define  	_DEBUG_					1	/*1: Debug on, 0: Debug off*/

#define 	TRUE					1
#define 	FALSE					0
#define 	P_RS_TX					_pa0
#define 	PC_RS_TX				_pac0		
#define 	PU_RS_TX				_papu0	

#define 	PA_RED_LED				_pa5				
#define		_RED_LED_ON()			PA_RED_LED = 0
#define 	_RED_LED_OFF()	    	PA_RED_LED = 1

#define 	C_LONG_DELAY   	  		122
#define	 	C_LONG_SP_DELAY    		90
#define 	_1_MINUTE_PERIOD		59	//60
#define		_BATTERY_PERIOD			60	//60
#define 	_30_SECONDS_PERIOD		30

#define		TASK_NUMBER				15		//14  12
#define		LED_BLINK_WIDTH			30
#define		LED_BLINK_OFF_WIDTH		350
#define		ALARM_MEMORY_EXIT		(u16)3600		// 1 hour timeout, 15 sec each red led blink.
#define		ALARM_MEMORY_PERIOD		15
#define		STUCK_BTN_PERIOD		4
#define		PHOTO_SUPV_UPPER_LIMIT	200
#define		MEMORY_ERROR_CODE		8			//3
#define 	MEMORY_ERROR_CODE_MODE	3
#define		PHOTO_ERROR_CODE		4
#define		PTT_ERROR_CODE			7
#define		EOL_CODE				2


//=======================================
//2021-8-2
#define		_SMOKE_ALARM			0
#define		_HUSH_ACTIVE			1
#define		_EOL_FATAL				2
#define		_PHOTO_FAULT			3
#define		_MEMORY_FAULT			4
#define		_BTN_STUCK				5
#define  	_LIFE_EOL				6
#define		_BATTERY_LOW			7
#define		_ALARM_MEMORY			8
#define		_BATTERY_HUSH_ACTIVE	9
#define		_EOL_HUSH				10
#define		_PTT_ACTIVE				11
#define		_IN_CAL_STATUS			12
#define		_PHOTO_CAL_FAILURE		13


/*
- Priority of Activities:
0. Smoke Alarm
1. Smoke Alarm Hush
2. Photo Fault
3. Flash Fatal Fault
4. Stuck Button
5. Low Battery Fatal
6. Low Battery
7. Alarm Memory
8. Low Battery Hush 
*/
extern word_type task_priority;
#define		FLAG_SMOKE_ALARM			task_priority.bits.bit0
#define		FLAG_HUSH_ACTIVE			task_priority.bits.bit1 //
#define		FLAG_EOL_FATAL				task_priority.bits.bit2
#define		FLAG_PHOTO_FAULT			task_priority.bits.bit3
#define		FLAG_MEMORY_FAULT			task_priority.bits.bit4
#define		FLAG_BTN_STUCK				task_priority.bits.bit5
#define		FLAG_EOL					task_priority.bits.bit6
#define		FLAG_BATTERY_LOW			task_priority.bits.bit7
#define		FLAG_ALARM_MEMORY			task_priority.bits.bit8//
#define		FLAG_BATTERY_HUSH_ACTIVE	task_priority.bits.bit9
#define		FLAG_EOL_HUSH				task_priority.bits.bit10
#define		FLAG_PTT_ACTIVE				task_priority.bits.bit11
#define		FLAG_IN_CAL_STATUS			task_priority.bits.bit12
#define		FLAG_PHOTO_CAL_FAILURE		task_priority.bits.bit13
#define		FLAG_PRE_ALARM				task_priority.bits.bit14
#define		FLAG_FAST_SAMPLING			task_priority.bits.bit15


extern byte_type flag1;    //
#define		F_ALARM_MODE		flag1.bits.bit0	// Alarm mode
#define		FLAG_STANDBY		flag1.bits.bit1 // Standby mode
#define		F_NOR_CNT_MODE  	flag1.bits.bit2	// Standby counter
#define		FLAG_STUCK_MODE		flag1.bits.bit3	// Stuck mode
#define		FLAG_PHOTO_CAL		flag1.bits.bit4	// Calibration mode
#define		F_MEMORY_FAULT		flag1.bits.bit5 // memory fault
#define		FLAG_LB_HUSH_INIT	flag1.bits.bit6 // low battery init
#define		FLAG_DRIFT_FAULT	flag1.bits.bit7


extern byte_type flag2;
#define		FLAG_IDLE0_ACTIVE		flag2.bits.bit0		// Idle 0 valid
#define		FLAG_ACTIVITY_ALVIE		flag2.bits.bit1		// Activity alive
#define		FLAG_POWER_ON			flag2.bits.bit2		//ANCHOR flag of 5 min power on
#define		FLAG_LB_HUSHABLE		flag2.bits.bit3


extern byte_type flag3;
#define		FLAG_EE_FAULT			flag3.bits.bit0
#define		FLAG_ROM_FAULT			flag3.bits.bit1		// ROM checksum error
#define		F_ALMMORY_MODE			flag3.bits.bit3		// Alarm memory mode
#define		FLAG_PTT_FAULT			flag3.bits.bit4


extern u8 adc_conv_data[2];	
extern u8 photo_sampling_cnt;	
extern u16 _1_min_bat_hush_cnt;
extern u16 _1_sec_alarm_memory_cnt;
extern u16 _1_min_eol_hush_cnt;

void Clear_Horn_Status(void);
void Red_LED_Blink(u16 ms);
void Horn_Chirp_LED_Blink(u16 ms);
void Horn_Chirp(u16 ms);
void Task_Sched(void);
void Delay_Milliseconds(u16 cnt);
void Uart_Power_On_Output(void);
void Uart_CMD_Process(void);

void Calibration_Mode(void);
void Alarm_Hush_Mode(void);
void Photo_Fualt_Mode(void);
void Memory_Fault_Mode(void);
void Stuck_Button_Mode(void);
void Low_Battery_Hush_Mode(void);
void Alarm_Memory_Mode(void);
void Low_Battery_Mode(void);
void Standby_Mode(void);
void Uart_Opamp_Debug_Output(void);
void Uart_Set_Fast_Sampling(void);
void Uart_Revise_EE_Data(void);
void Uart_G_Dump(void);
#endif
