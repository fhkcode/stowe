/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/

#include "adc.h"


/**************************************************************************************************************************
													ADC_Init
*Description:
	SDAC0 register:	
		bit7 			START
		bit6 			ADBZ
		bit5 			ADCEN
		bit4 			ADRFS	
		bit3~bit0		0000 = AN0, 
						0001 = AN1, 
						0010 = AN2, 
						0011 = AN3, 
						0100~111 = Floating
						011=OPA1O	
						01 = [Vref=VDD]	
						011 = fsys/8 (1MHz)
	SDAC1 register:	
		bit7~bit5	011=OPA1O
		bit4~bit3	01 = [Vref=VDD]		
		bit2~bit0	011 = fsys/8 (1MHz)
								
*Arguments:		CH			channel number of ADC
				ADC_BIT		0 indicates 8 bit, 1 indicates 12 bit ADC output
*Return: 	None
***************************************************************************************************************************/
void ADC_Init()
{
	_sadc0=0b00000111 ; 

	_sadc1=0b01101011 ; 

}

/**************************************************************************************************************************
													Read_ADC_Channel
*Description:
*Arguments:		CH 			Channel number of ADC
				ADC_BIT 	0 indicates 8 bit, 1 indicates 12 bit ADC output
*Return: 	None
***************************************************************************************************************************/
unsigned int  Read_ADC_Channel(unsigned char CH,unsigned char ADC_BIT)
{
	unsigned int R16_ADC_DATA;
	
	switch(CH)
	{
		case 0:
			_RD_ADC_SET_AN0();
			break;
		
		case 1:
			_RD_ADC_SET_AN1();
			break;

		case 2:
			_RD_ADC_SET_AN2();
			break;
		
		case 3:
			_RD_ADC_SET_AN3();
			break;
		
		case 4:
			_RD_ADC_SET_VBGREF();
			break;
		
		case 5:
			_RD_ADC_SET_OPA0O();
			break;	
					
		case 6:
			_RD_ADC_SET_OPA1O();
			break;
		
		case 7:
			_RD_ADC_SET_LINEV();
			break;
		
		default:
			break;		
	}

	_adcen=1;
	GCC_DELAY(12);
	_start=0;
	_start=1;
	_start=0;
	GCC_DELAY(12);
	while(_adbz);
	_adcen=0;
	R16_ADC_DATA=_sadoh;
	if(ADC_BIT == ADC_12BIT)
	{
		R16_ADC_DATA=(R16_ADC_DATA<<8)| _sadol;
	}
	else
	{
		R16_ADC_DATA = (R16_ADC_DATA<<4) | (_sadol>>4);		// use the high 8 bit data.
	}
	return R16_ADC_DATA;
}
