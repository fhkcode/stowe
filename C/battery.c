/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/
#include "board.h"
#include "sys.h"
#include "adc.h"
#include "eeprom.h"
#include "uart.h"
#include "battery.h"
#include "photo.h"


u16 battery_volt = 0;
u16 battery_test_count = 0;

/**************************************************************************************************************************
													Battery_Test_Current_On
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Battery_Test_Current_On(void)
{
	_isgen = 1;
	_isgs1 = 1;
}

/**************************************************************************************************************************
													Battery_Test_Current_Off
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Battery_Test_Current_Off(void)
{
	_isgen = 0;
	_isgs1 = 0;
}

/**************************************************************************************************************************
													Read_Battery_Volt
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
unsigned int Read_Battery_Volt(void)
{
	u16 temp_bat_volt = 0;
	
	_sadc1 = 0b00001010;
	_sadc0 = 0b00110010;
	_start = 0;
	_start = 1;
	_start = 0;
	GCC_DELAY(4000);

	_isgen = 1;
	_isgs1 = 1;
	//30ms
	GCC_DELAY(30000);
	GCC_DELAY(30000);
	_isgen = 0;
	_isgs1 = 0;

	_start = 0;
	_start = 1;
	_start = 0;
	while(_adbz);
	adc_conv_data[1] = _sadoh;
	adc_conv_data[0] = _sadol;	
	//12bit ADC
	temp_bat_volt = (u16)adc_conv_data[1] << 8 | adc_conv_data[0];
	

	return temp_bat_volt;
}

/**************************************************************************************************************************
													Battery_Pulse
*Description:
	Battery test, if battery voltage lower than 7.4~7.6VDC, enters low battery mode, this mode can be hush.
2 cycles for battery test, dwell time 20ms.
if battery voltage < 7.4V, it enters low battery fatal, this mode cannot be hushed.
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Battery_Pulse(void)
{	
	u16 Battery_volt1,Battery_volt2;


	Battery_volt1 = Read_Battery_Volt();

	u16 i;
	for (i = 0; i < 20; i++)
	{
		GCC_DELAY(2000);	/*10 ms delay*/
	}
	Battery_volt2 = Read_Battery_Volt();
	
	//get max voltage
	if(Battery_volt2 >= Battery_volt1)
	{
		battery_volt = Battery_volt2;
	}	
	else
	{
		battery_volt = Battery_volt1;
	}
		
	if((battery_volt > BATTERY_FATAL_LIMIT)&&(battery_volt <= BATTERY_WARNNING_LIMIT))
	{
		if((FLAG_BATTERY_LOW == FALSE) && (FLAG_BATTERY_HUSH_ACTIVE == FALSE))
		{
			FLAG_BATTERY_LOW = TRUE;
			FLAG_LB_HUSHABLE = TRUE;
			Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_LB_FAULT);			
		}
		Uart_Send_String("LB\r");
	}
	else if(battery_volt <= BATTERY_FATAL_LIMIT){
		FLAG_LB_HUSHABLE = FALSE;
		FLAG_BATTERY_LOW = TRUE;
		FLAG_BATTERY_HUSH_ACTIVE = FALSE;
		Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_LB_FAULT);
		Uart_Send_String("LBF\r");
	}else{
		FLAG_LB_HUSHABLE = FALSE;
		FLAG_BATTERY_LOW = FALSE;	
	}	

	Uart_Send_String(">B:\0");
	_itoa((u16) battery_volt, 10);
	Uart_Send_String(",L:\0");
	_itoa((u16) BATTERY_WARNNING_LIMIT, 10);
	Uart_Send_Byte('\r');	
}


/**************************************************************************************************************************
													Battery_Test
*Description:	When battery voltage going down to 7.4V~7.6V, enter low battery mode, in this mode the alarm will chirp every 60 seconds, 
				blink red LED every 30 seconds. The alarm can be hushed, hush will be timeout after 24 hours.When battery voltage < 7.4V, 
				this alarm cannot be hushed any more.
				In the 1st 12 minutes after power on , do the battery test every 1 minute, after 12 minutes, do battery test every 12 hours
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Battery_Test(void)
{
	/*static u16 battery_test_count = 0;*/
	static u8 _12_min_battery_test_ = 0;

	battery_test_count++;
	
	if(FLAG_POWER_ON) 
	{
		if(battery_test_count >= _BATTERY_PERIOD)
		{
			Battery_Pulse();
			battery_test_count = 0;
			if((_12_min_battery_test_ ++) >= _12_MIN_TIMEOUT)
			{
				FLAG_POWER_ON = FALSE;	
			}	
		}	
	}
	else
	{
		if( battery_test_count >= _12_HOURS)	// 12 hours check period.
		{
			battery_test_count = 0;	
			Battery_Pulse();
		}	
	}
}

