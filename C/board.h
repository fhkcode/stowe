/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/

#ifndef _BOARD_H__
#define _BOARD_H__

#include "BA45F5440.h"
#include "sys.h"


#define     PA4_CTRL	                _pac4
#define     PA4_PULLUP	                _papu4
#define     PA4_HORN_TEST	            _pa4

#define 	_AC_COUPLING	            0               
// #define 	C_RAM_ADDR_START	        0x80		    // RAM starting address
// #define 	C_RAM_ADDR_END		        0xFF		    // RAM Last address
#define 	ROM_ADDR_RANGE	        (u16)0x0FFF		// ROM Last address
#define		_S_OPA_DELAY()		        {GCC_DELAY(2000);}	// 2000 count = 1ms	
#define     _1_DAY_MINUTES_             (u16)1439      // 1440 minutes per 24 hours. 1440
#define		_1_Day_EOL_MINS_			(u16)2800			// 28801440 minutes per 24 hours.
#define     LIFE_EXPIRATION              3750
#define		LFE_UL_EXTENSION 			(LIFE_EXPIRATION + 7)


extern u8 _1_sec_counter;			
extern u16 battery_volt;			
extern u16 _life_cycles;
extern u8 _life_seconds_counter;
extern u16 _life_minutes_counter;
extern u8 ee_data;
extern u16 Time_Rate;
extern u16 Comp_Rate;

void RAM_Init(u8 RAN_ADD);
void System_Init(void);
void Opamp_Init(void);
void Opamp_0_Calibration(void);
void Opamp_1_Calibration(void);
void Read_ADC(void);
unsigned int Read_ROM_Data(unsigned int R_addr);
void ROM_Data_Validate(void);
void Board_Init(void);	
void Power_On_Blink(void);
void Life_Update(void);
void EOL_Mode(void);
void EOL_HUSH_Mode(void);
void EOL_Fatal_Mode(void);
#endif








