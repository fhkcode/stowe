/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/ 

#include "sys.h"
#include "BA45F5440.h"
#include "horn.h"
#include "button.h"
#include "board.h"
#include "photo.h"
#include "adc.h"
#include "uart.h"
#include "eeprom.h"
#include "battery.h"

byte_type	flag1 = {.byte = 0};
byte_type	flag2 =  {.byte = 0};
byte_type 	flag3 = {.byte = 0};
word_type	task_priority = {.word = 0};

u8 photo_sampling_cnt = 0;
u16 _1_min_bat_hush_cnt = 0;
u16 _1_sec_alarm_memory_cnt = 0;
u16 _1_min_eol_hush_cnt = 0;
static u8 stuck_btn_cnt = 0;
u16 Time_Rate = _1_DAY_MINUTES_;


/**************************************************************************************************************************
													Delay_Milliseconds
*Description:	1ms delay function
*Arguments: 
			cnt		how many ms to be delay.
*Return: 	None
***************************************************************************************************************************/
void Delay_Milliseconds(u16 cnt)
{
	u16 i;
	for (i = 0; i < cnt; i++)
	{
		GCC_DELAY(2000);	/*1 ms delay*/
	}	
}

/**************************************************************************************************************************
													Clear_Horn_Status
*Description:	Clear horn status bits, stop the sounder to be ready for next alarm signal.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Clear_Horn_Status(void)
{
	flag_horn_alarm_bits.byte = 0;
	_OFF_BUZZ();
	FLAG_HORN_ALARM_ENABLE = FALSE;
	_RED_LED_OFF();
}


/**************************************************************************************************************************
													Uart_Power_On_Output
*Description:	Output firmware revision, photo supervision, alarm threshold etc.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Uart_Power_On_Output(void)
{
	if(_to == 1)
	{
		Uart_Send_String("\r\rWTD Reset\r");
	}
	else
	{
		Uart_Send_String("\r\rPOR\r");
	}
	Uart_Send_String("\r>Model:2030-PG01\r");
	Uart_Send_String(FW_REV);
	Uart_Send_String(">supv:");
	_itoa((u16)eeprom._photo_supv, 10);
	Uart_Send_String(", alarm:");
	_itoa((u16)eeprom._corrected_alarm_thd, 10);
	Uart_Send_Byte('\r');

	if(FLAG_EE_FAULT || FLAG_ROM_FAULT)
	{
		Uart_Send_String(">Mem fault\r");
		FLAG_MEMORY_FAULT  = TRUE;
	}
	else
	{
		Uart_Send_String(">Sum calc OK\r");
	}
}

/**************************************************************************************************************************
													Red_LED_Blink
*Description:	Blink red LED with given times.
	ms -- expected delay time, unit in microseconds.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Red_LED_Blink(u16 ms)
{
	_RED_LED_ON();
	Delay_Milliseconds(ms);
	_RED_LED_OFF(); 
}

/**************************************************************************************************************************
													Horn_Chirp_LED_Blink
*Description:	Sounder chirps and Red LED blinks with given times.
*Arguments: 
	ms -- expected delay time, unit in microseconds.
*Return: 	None
***************************************************************************************************************************/
void Horn_Chirp_LED_Blink(u16 ms)
{
	_RED_LED_ON();
	_OPEN_BUZZ();
	Delay_Milliseconds(ms);
	_OFF_BUZZ();
	_RED_LED_OFF();
}

/**************************************************************************************************************************
													Horn_Chirp
*Description:	Sounder chirps with given times.
*Arguments: 
	ms -- expected delay time, unit in microseconds.
*Return: 	None
***************************************************************************************************************************/
void Horn_Chirp(u16 ms)
{
	_OPEN_BUZZ();
	Delay_Milliseconds(ms);
	_OFF_BUZZ();
}

/**************************************************************************************************************************
													Task_Sched
*Description:	Task schedule function, Priority from top to bottom are:		
			#define		_SMOKE_ALARM			0
			#define		_HUSH_ACTIVE			1
			#define		_EOL_FATAL				2
			#define		_PHOTO_FAULT			3
			#define		_MEMORY_FAULT			4
			#define		_BTN_STUCK				5
			#define  	_LIFE_EOL				6
			#define		_BATTERY_LOW			7
			#define		_ALARM_MEMORY			8
			#define		_BATTERY_HUSH_ACTIVE	9
			#define		_EOL_HUSH				10
			#define		_PTT_ACTIVE				11
			#define		_IN_CAL_STATUS			12
			#define		_PHOTO_CAL_FAILURE		13
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Task_Sched(void)
{
	u8	task_id = 0xff;
	if (task_priority.word & 0xffff){
		for(task_id=0; task_id < TASK_NUMBER; task_id++){
			if(task_priority.word & (1<<task_id))
				break;
		}
	}
	
	switch(task_id)
	{
		case _SMOKE_ALARM:
			break;
		case _PTT_ACTIVE:
			Push_To_Test_Check();
			break;
		case _HUSH_ACTIVE:
			Alarm_Hush_Mode();	/*Alarm Hush mode Flashes once every 2 seconds*/
			break;
		case _EOL_FATAL:
			EOL_Fatal_Mode();
			break;
		case _PHOTO_FAULT:
			Photo_Fualt_Mode();
			break;
		case _MEMORY_FAULT:
			 Memory_Fault_Mode();	/*Memory fault, LED blink 3 times and sounder chirp every 30 sec.	*/
			break;
		case _BTN_STUCK:			
			Stuck_Button_Mode();	//Stuck button mode 4s period
			break;	
		case _BATTERY_HUSH_ACTIVE:
			Low_Battery_Hush_Mode();	/*Low battery Hush mode, flashing red light every 30s*/
			break;
		case _LIFE_EOL:
			EOL_Mode();
			break;
		case _ALARM_MEMORY:
			Alarm_Memory_Mode();
			break;
		case _BATTERY_LOW:
			 Low_Battery_Mode();
			break;
		case _IN_CAL_STATUS:
			Calibration_Mode();
			break;
		case _PHOTO_CAL_FAILURE:
			Clear_Horn_Status();
			_RED_LED_ON();
			break;
		case _EOL_HUSH:
			EOL_HUSH_Mode();
			break;
		default:
			Standby_Mode();	/*Standby mode Red light flashes once in 60s, if there is low voltage, the buzzer sounds once*/
			break;	
	}
}

/**************************************************************************************************************************
													Calibration_Mode
*Description:	If device is uncalibrated, after power on, firmware will run the calibration process. In the calibration process
				the red LED will blink ON and OFF ever 1 second, and firmware will sample the photo output 1 second / count, until
				it reaches the maximum sampling number.
				If calibraton is valid, firmware will caculate the alarm threshold, then save the averaged CAV, alarm threshold into
				EEPROM.
				If calibration failed, the red LED will solid on to inidicate the failure.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Calibration_Mode(void)
{
	PA_RED_LED = (_1_sec_counter % 2) ? 1 : 0;
	Smoke_Calibration();
}


/**************************************************************************************************************************
													Alarm_Hush_Mode
*Description:	If alarm hush is valid, then firmware will enter a hush mode state, in this state, the device cannot re-enter a smoke alarm.
				Timeout = 9 minutes, after 9 minutes then smoke alarm is enabled if the smoke condition still exit.
				During the alarm device in hushed, the alarm device won't respond to the smoke alarm, even the smoke concentration is higher.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Alarm_Hush_Mode(void)
{
	FLAG_T3_ALARM_STOP = TRUE;
	
	static u16 _1_sec_hush_counter = 0;
	if(FLAG_HUSH_ACTIVE)	//NOTE: hush activated.
	{
		_1_sec_hush_counter++;						
		if(_1_sec_hush_counter >= HUSH_9_MIN_TIMEOUT)	//NOTE: 9 miuntes timeout
		{
			FLAG_HUSH_ACTIVE = FALSE;
			_1_sec_hush_counter = 0;	
			Uart_Send_String("Hush Ends\r");
		}
		else if(_1_sec_hush_counter % 2)
		{
			Red_LED_Blink(LED_BLINK_WIDTH);
		}
	}	
}

/**************************************************************************************************************************
													Photo_Fualt_Mode
*Description:	If the photo output is lower than the supervision limit (30 counts), then the unit will enter a photo fault mode,
				the Red LED will blink 4 times every 30 seconds, and sounder chirp every 30 seconds, until the fualt is removed.
				Button pressed will blink the error code, and reset the alarm device.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Photo_Fualt_Mode(void)
{
	Clear_Horn_Status();
	if(_1_sec_counter >= _30_SECONDS_PERIOD)
	{
		Uart_Send_String("Photo fault\r");
		_1_sec_counter = 0;				
		unsigned char i;
		for(i=0;i<PHOTO_ERROR_CODE;i++)
		{
			Red_LED_Blink(LED_BLINK_WIDTH);
			Delay_Milliseconds(LED_BLINK_OFF_WIDTH);
		}
		Horn_Chirp(LED_BLINK_WIDTH);	
	}
}

/**************************************************************************************************************************
													Memory_Fault_Mode
*Description:	If the checksum of program data and EEPROM data doesn't match to the value stored in the EEPROM, then alarm
				device will enter memory fault, Red LED will blink 3 times every 30 seconds, and sounder chirps every 30 seconds.
				Button pressed will blink the error code, and reset the alarm device.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Memory_Fault_Mode(void)
{
	FLAG_T3_ALARM_STOP = FALSE;
	if(F_MEMORY_FAULT == FALSE)
	{
		Clear_Horn_Status();
		F_MEMORY_FAULT = TRUE;
		_1_sec_counter = 0;
	}
	if( _1_sec_counter >= _30_SECONDS_PERIOD )
	{
		_1_sec_counter = 0;
		unsigned char i;
		for(i=0;i<MEMORY_ERROR_CODE_MODE;i++)
		{
			Red_LED_Blink(LED_BLINK_WIDTH);
			Delay_Milliseconds(LED_BLINK_OFF_WIDTH);
		}
		Horn_Chirp(LED_BLINK_WIDTH);
	}
}

/**************************************************************************************************************************
													Stuck_Button_Mode
*Description:	If the test button is pressed over 4 seconds, then the alarm device will give a fault, souder and Red LED 
				chirps/ blinks every 4 seconds.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Stuck_Button_Mode(void)
{
	if( FLAG_STUCK_MODE == FALSE)
	{
		Clear_Horn_Status();
		FLAG_STUCK_MODE = TRUE;
		Uart_Send_String("BTN STK\r");
	}
	stuck_btn_cnt++;
	if( stuck_btn_cnt >= STUCK_BTN_PERIOD)
	{
		stuck_btn_cnt = 0;
		Horn_Chirp_LED_Blink(LED_BLINK_WIDTH);
	}
}

/**************************************************************************************************************************
													Low_Battery_Hush_Mode
*Description:	if the battery voltage is within 7.4V~7.6V, the alarm device can be hushed, during the low battery hush mode,
				the alarm device will blink the every 30 seconds, but no sounder chirps.
				Button press will exit the low battery hush mode.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Low_Battery_Hush_Mode(void)
{
	if(FLAG_LB_HUSH_INIT == FALSE)
	{
		flag_horn_alarm_bits.byte = 0;
		_OFF_BUZZ();
		FLAG_HORN_ALARM_ENABLE = FALSE;
		_RED_LED_OFF();
		FLAG_LB_HUSH_INIT = TRUE;
		_1_sec_counter = 0;
	}
	if(_1_sec_counter >= _30_SECONDS_PERIOD)
	{
		_1_sec_counter = 0;
		_RED_LED_ON();
		u16 i;
		for (i = 0; i < LED_BLINK_WIDTH; i++)
		{
			GCC_DELAY(2000);	/*1 ms delay*/
		}	
		_RED_LED_OFF(); 
		_1_min_bat_hush_cnt ++;
		if( _1_min_bat_hush_cnt >= LOW_BAT_HUSH_TIMEOUT)
		{
			_1_min_bat_hush_cnt = 0;
			battery_test_count = 0;
			FLAG_BATTERY_HUSH_ACTIVE = FALSE;
			Battery_Pulse();
		}
	}
}

/**************************************************************************************************************************
													Alarm_Memory_Mode
*Description:
	When the product exits the alarm, it will trigger an alarm memory function, that is, the product flashes a red light 
every 15 seconds without squeaking. After 1 hours, the product will automatically exit the alarm memory or press the button 
within 24 hours to cancel the alarm memory.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Alarm_Memory_Mode(void){
	FLAG_T3_ALARM_STOP = TRUE;
	if(FLAG_HORN_ALARM_ENABLE == FALSE)
	{
		if(F_ALMMORY_MODE == FALSE)
		{
			F_ALMMORY_MODE = TRUE;
			_1_sec_alarm_memory_cnt = 0;
		}
		else
		{
			if(_1_sec_alarm_memory_cnt >= ALARM_MEMORY_EXIT)
			{
				FLAG_ALARM_MEMORY = FALSE;
				F_ALMMORY_MODE = FALSE;
				_1_sec_alarm_memory_cnt = 0;
			}
			else if((_1_sec_alarm_memory_cnt % ALARM_MEMORY_PERIOD) == 0)
			{
				Red_LED_Blink(LED_BLINK_WIDTH); 
			}
			_1_sec_alarm_memory_cnt ++;
		}	
	}
}

/**************************************************************************************************************************
													Low_Battery_Mode
*Description:	if the battery voltage lower than 7.6VDC, then alarm device will blink the Red LED every 30 seconds, and 
				sounder chirps every 60 seconds. Until the voltage recovers to 7.6VDC above.
				Battery test current will be 30mA. 
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Low_Battery_Mode(void){
	if(_1_sec_counter == _30_SECONDS_PERIOD)
	{
		Red_LED_Blink(LED_BLINK_WIDTH);	
	}
	else if(_1_sec_counter >= _1_MINUTE_PERIOD)
	{
		_1_sec_counter = 0;
		Horn_Chirp_LED_Blink(LED_BLINK_WIDTH);
	}
}

/**************************************************************************************************************************
													Standby_Mode
*Description:	Under standby mode, the Red LED will blink every 60 seconds.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Standby_Mode(void){
	if(FLAG_HORN_ALARM_ENABLE == FALSE)
	{
		if(FLAG_STANDBY == FALSE)
		{
			Clear_Horn_Status();
			FLAG_STANDBY = TRUE;	
			_1_sec_counter = 0;
		}
		if(_1_sec_counter >= _1_MINUTE_PERIOD)
		{
			_1_sec_counter = 0;	
			Red_LED_Blink(LED_BLINK_WIDTH);
		}
	}
}

/**************************************************************************************************************************
													Uart_CMD_Process
*Description:	Currently support below commands:
		1. c1 	command to revise EE data,
		2. C0 	command to print FW version, NOT USED.
		3. G 	command to dump EE data
		4. b 	command for battery test
		5. AN 	turn on OPAMP 1 output to PA5
		6. A1 	command, fast sampling mode
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Uart_CMD_Process(void)
{
	unsigned char uart_cmd = 0;

	if(FLAG_COMMD_VAILD)
	{
		if( (rx_buffer[0] == 'c') && (rx_buffer[1] == '1') )		uart_cmd = CMD_REVISE_EE;	//
		if( (rx_buffer[0] == 'G') )									uart_cmd = CMD_GDUMP;	//
		if( (rx_buffer[0] == 'b') )									uart_cmd = CMD_BAT_TEST;	//
		if( (rx_buffer[0] == 'A') && (rx_buffer[1] == 'N') )		uart_cmd = CMD_OPAMP_DEBUG;	// 
		if( (rx_buffer[0] == 'A') && (rx_buffer[1] == '1') )		uart_cmd = CMD_FAST_MODE;	//
		if( (rx_buffer[0] == 'L') && (rx_buffer[1] == '1') )		uart_cmd = CMD_SET_SPEED;	//
		
		switch(uart_cmd)
		{				
			case CMD_REVISE_EE:
				Uart_Revise_EE_Data();
				break;
			case CMD_GDUMP:
				Uart_G_Dump();
				break;
			case CMD_BAT_TEST:
				if(rx_buffer[1] == '\r')
					Battery_Pulse();
				break;
			case CMD_OPAMP_DEBUG:
				Uart_Opamp_Debug_Output();
				break;
			case CMD_FAST_MODE:
				Uart_Set_Fast_Sampling();
				break;
			case CMD_SET_SPEED:
				Time_Rate = 1;
				Comp_Rate = 60;
				break;		
			default:
				break;
		}
		Uart_RX_Buffer_Clear();
	}	
	else if( rx_count >= (BUFFER_LIMIT - 1))
		Uart_RX_Buffer_Clear();
}

/**************************************************************************************************************************
													Uart_Opamp_Debug_Output
*Description:
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Uart_Opamp_Debug_Output(void){
	if(rx_buffer[2] == '\r')
	{
		_pas12 = _pas12? 0 : 1;
	}
}

/**************************************************************************************************************************
													Uart_Set_Fast_Sampling
*Description:
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Uart_Set_Fast_Sampling(void){
	if(rx_buffer[2] == '\r')
	{
		FLAG_FAST_SAMPLING = FLAG_FAST_SAMPLING? 0 : 1;
	}
}

/**************************************************************************************************************************
													Uart_Revise_EE_Data
*Description:
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Uart_Revise_EE_Data(void){
	unsigned char _addr=0,_data=0;
	int diff = 0;

	if(rx_buffer[8] == '\r')
	{
		_addr = Uart_Ascii_To_Hex( rx_buffer[3],rx_buffer[4] );
		_data = Uart_Ascii_To_Hex( rx_buffer[6],rx_buffer[7] );
		
		diff = _data - (int)Read_EE_Data(_addr);
		Write_EE_Data(_addr, _data);
		Write_EE_Data(_addr + BACKUP_START, _data);
		eeprom._sum = ((int)eeprom._sum) + diff;
		Save_Checksum(eeprom._sum);
		Load_Non_Volatile_data();
		
	}

}

/**************************************************************************************************************************
													Uart_G_Dump
*Description:
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Uart_G_Dump(void)
{
	unsigned char i;
	if(rx_buffer[1] == '\r')
	{
		Uart_Send_String("H:UU");
		for(i=0; i < 64; i++)
		{
			if((i % 16) == 0)
			{
				Uart_Send_Byte('\r');
				_itoa(i, 16);
				Uart_Send_String(": ");
			}
			_itoa(Read_EE_Data(i), 16);
			Uart_Send_Byte(' ');
		}
		Uart_Send_Byte('\r');
	}
}
